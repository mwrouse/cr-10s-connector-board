# CR-10S Connector Board

Add-on board for your CR-10S that allows for all the wires to be plugged in to the back of the control box, rather than the wires coming out of the control box. 

The board also implements TL smoothing on all stepper motors.